#ifndef CMD_CHECK
#define CMD_CHECK

#include "command.h"
#include "m3_config.h"

class m3_check: public Command
{
public:
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif