#ifndef COMMAND
#define COMMAND

#define TOOL_NAME "m3"
//this is here so everything that needs it, including the main file, can see it

class Command {
	public:
		virtual int run(int argc, char** argv) = 0;
		virtual int help(char* cmdname) = 0;
		virtual void usage(char* cmdname) = 0;
};

#endif
