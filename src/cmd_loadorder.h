#ifndef CMD_LOADORDER
#define CMD_LOADORDER

#include "command.h"
#include "m3_config.h"

class m3_loadorder: public Command
{
public:
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif