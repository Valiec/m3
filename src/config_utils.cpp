#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include "config_utils.h"

bool DataFolder::has_mod(std::string name)
{
	for(auto mod : this->content)
	{
		if(mod.name == name)
		{
			return true;
		}
	}
	return false;
}

bool DataFolder::has_bsa(std::string name)
{
	for(auto bsa : this->bsas)
	{
		if(bsa.name == name)
		{
			return true;
		}
	}
	return false;
}

bool is_valid_mod_name(std::string str)
{
	std::string lowerName(str);
	lowercaseStr(lowerName);
	return (str_endswith(lowerName, ".esm") || str_endswith(lowerName, ".esp") || str_endswith(lowerName, ".omwaddon"));
}

bool is_bsa(std::string str)
{
	std::string lowerName(str);
	lowercaseStr(lowerName);
	return (str_endswith(lowerName, ".bsa"));
}

bool str_endswith(std::string str, std::string substr)
{
	int substr_len = substr.size();
	int str_len = str.size();
	if(substr_len > str_len)
	{
		//suffix is longer than string, cannot end with this
		return false;
	}
	std::string cmpstr(str, str_len-substr_len, str_len);
	return cmpstr == substr;
}

int match_mod_to_directory(ContentFile &mod, std::vector<std::string> &dir_names, std::unordered_map<std::string, DataFolder> &dirs)
{
	for(int i=0; i<dir_names.size(); i++)
	{
		DataFolder folder = dirs.at(dir_names[i]);
		std::filesystem::path path(folder.path);

		path/=mod.name;

		if(std::filesystem::exists(path))
		{
			return i;
		}
	}
	return -1;
}

void lowercaseStr(std::string &str)
{
	for(int i=0; i<str.size(); i++)
	{
		char c = str.at(i);
		str.at(i) = tolower(c);
	}
}

ContentFile::ContentFile(std::string name)
{
	this->name = name;
	std::string ext(name, name.find_last_of('.')+1);
	lowercaseStr(ext);
	this->extension = ext;
	this->isBaseGame = is_base_morrowind(name);
	this->active = false;

}

void ContentFile::activate()
{
	this->active = true;
}

void ContentFile::deactivate()
{
	this->active = false;
}

BSAFile::BSAFile(std::string name)
{
	this->name = name;
	this->isBaseGame = is_base_morrowind(name);
	this->active = false;
}

void BSAFile::activate()
{
	this->active = true;
}

void BSAFile::deactivate()
{
	this->active = false;
}

DataFolder::DataFolder(std::string name)
{
	this->name = name;
	this->path = "<MISSING>";
	this->active = false;
}

DataFolder::DataFolder(std::string name, std::string path)
{
	this->name = name;
	this->path = path;
	this->active = false;
}

void DataFolder::activate()
{
	this->active = true;
}

void DataFolder::deactivate()
{
	this->active = false;
}


bool is_base_morrowind(std::string name)
{
	std::string cmpstr(name, 0, name.size()-4); //no extension
	if(cmpstr == "Morrowind" || cmpstr == "Tribunal" || cmpstr == "Bloodmoon")
	{
		return true;
	}
	return false;
}