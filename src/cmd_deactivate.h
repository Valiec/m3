#ifndef CMD_DEACTIVATE
#define CMD_DEACTIVATE

#include "command.h"
#include "m3_config.h"

class m3_deactivate: public Command
{
public:
	int deactivate(char* mod, M3ConfigFile &config, OMWConfigFile &omw_config);
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif