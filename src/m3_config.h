#ifndef M3_CONFIG_PARSE
#define M3_CONFIG_PARSE

#include <string>
#include <vector>
#include <unordered_map>
#include "config_utils.h"
#include "omw_config.h"

class M3ConfigFile
{
public:
	std::string omw_config_path;
	std::string base_game_path;
	std::vector<std::pair<std::string, int>> bsa_load_order;
	std::vector<std::string> data_folder_names;
	std::unordered_map<std::string, DataFolder> data_folders;
	std::vector<std::pair<std::string, int>> load_order;
	int status;

	//M3ConfigFile(std::string filename);
	M3ConfigFile(OMWConfigFile cfg);
	M3ConfigFile(std::string filename);
	int parse(std::string filename);
	int save(std::string filename);
	void apply(OMWConfigFile &omw_config);
	bool has_data_folder(std::string dir_path);
	std::string get_data_folder(std::string dir_path);
	std::string add_data_folder(DataFolder& folder);
	void remove_data_folder(DataFolder& folder);
	void print();
private:
	void format(std::ostream &f);
};

class ConfigLine
{
public:
	std::string name;
	std::unordered_map<std::string, bool> tags;
	std::string value;

	ConfigLine(std::string linebuf);
};

#endif