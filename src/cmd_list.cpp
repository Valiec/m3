#include <iostream>
#include <string>
#include <filesystem>

#include "cmd_list.h"

int m3_list::run(int argc, char** argv)
{
	M3ConfigFile config("m3_config.cfg");

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	int i;
	for(i=0; i<config.data_folder_names.size(); i++)
	{
		int j;
		DataFolder data_folder = config.data_folders.at(config.data_folder_names[i]);
		std::cout << (data_folder.active ? "[Active] " : "") << data_folder.name << " (" << data_folder.path << ")" << std::endl; 
		for(j=0; j<data_folder.content.size(); j++)
		{
				std::filesystem::path content_path = std::filesystem::path(data_folder.path) / data_folder.content[j].name;
				std::cout << "\t" << (data_folder.content[j].active ? "[Active] " : "") << (data_folder.content[j].isBaseGame ? "[Base] " : "") << data_folder.content[j].name << std::endl; 
				
		}
	}


	return 0;

}

int m3_list::help(char* cmdname)
{
	return 0;
}
void m3_list::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << std::endl;
}