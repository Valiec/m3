#include <iostream>
#include <string>
#include <filesystem>

#include "cmd_install.h"

int m3_install::install(char* mod, M3ConfigFile &config)
{

	std::string mod_path_str(mod);

	if(mod_path_str.back() == '/' || mod_path_str.back() == '\\')
	{
		mod_path_str.pop_back(); //get rid of a trailing \ or /
	}	

	std::filesystem::path mod_path(mod_path_str);

	if(!std::filesystem::exists(mod_path))
	{
		std::cerr << "error: path '" << mod_path_str << "' does not exist" << std::endl;
		return 1;
	}

	bool is_dir = std::filesystem::is_directory(mod_path);

	std::string filename;
	std::string dirname;
	std::string dirpath;	

	if(!is_dir)
	{

		filename = mod_path.filename().string();
		dirname = mod_path.parent_path().filename().string();
		dirpath = mod_path.parent_path().string();
	}

	else
	{
		filename = "";
		dirname = mod_path.filename().string();
		dirpath = mod_path.string();
	}

	if(is_dir)
	{
		if(!config.has_data_folder(std::filesystem::canonical(dirpath)))
		{
			DataFolder new_dir = DataFolder(dirname, dirpath);

			std::filesystem::directory_iterator mod_files = std::filesystem::directory_iterator(dirpath);
			for(auto mod_file : mod_files)
			{
				std::filesystem::path mod_file_path = mod_file.path();
				if(is_valid_mod_name(mod_file_path.filename().string()))
				{
					ContentFile mod_entry = ContentFile(mod_file_path.filename());
					std::cout << "Content file '" << mod_entry.name << "' registered." << std::endl;
					new_dir.content.push_back(mod_entry);
				}
				if(is_bsa(mod_file_path.filename().string()))
				{
					BSAFile mod_entry = BSAFile(mod_file_path.filename());
					std::cout << "BSA file '" << mod_entry.name << "' registered." << std::endl;
					new_dir.bsas.push_back(mod_entry);
				}
			}
			std::string new_name = config.add_data_folder(new_dir);
			std::cout << "Folder '" << dirpath << "' added as " << new_name << std::endl;
		}
		else
		{
			std::cout << "Folder '" << dirpath << "' already registered, registering all contained mods." << std::endl;
			std::string data_folder_name = config.get_data_folder(std::filesystem::canonical(mod_path));
			DataFolder new_dir = config.data_folders.at(data_folder_name);
			std::filesystem::directory_iterator mod_files = std::filesystem::directory_iterator(dirpath);
			for(auto mod_file : mod_files)
			{
				std::filesystem::path mod_file_path = mod_file.path();
				if(is_valid_mod_name(mod_file_path.filename().string()) && (!new_dir.has_mod(mod_file_path.filename().string())))
				{
					ContentFile mod_entry = ContentFile(mod_file_path.filename());
					std::cout << "Content file '" << mod_entry.name << "' registered." << std::endl;
					new_dir.content.push_back(mod_entry);
				}
				if(is_bsa(mod_file_path.filename().string()) && (!new_dir.has_bsa(mod_file_path.filename().string())))
				{
					BSAFile mod_entry = BSAFile(mod_file_path.filename());
					std::cout << "BSA file '" << mod_entry.name << "' registered." << std::endl;
					new_dir.bsas.push_back(mod_entry);
				}
			}
		}
	}
	else
	{
		if(!config.has_data_folder(std::filesystem::canonical(dirpath)))
		{
			std::cout << "Folder '" << dirpath << "' not registered, registering..." << std::endl;

			DataFolder new_dir = DataFolder(dirname, dirpath);

			std::string new_name = config.add_data_folder(new_dir);
			std::cout << "Folder '" << dirpath << "' added as " << new_name << std::endl;
		}
		std::string data_folder_name = config.get_data_folder(std::filesystem::canonical(dirpath));
		DataFolder new_dir = config.data_folders.at(data_folder_name);

		if(is_valid_mod_name(filename))
		{
			if(new_dir.has_mod(filename))
			{
				std::cout << "Content file '" << filename << "' already registered! Skipping." << std::endl;
			}
			else
			{

				ContentFile mod_entry = ContentFile(filename);
				std::cout << "Content file '" << mod_entry.name << "' registered." << std::endl;
				new_dir.content.push_back(mod_entry);
			}
		}
		else if(is_bsa(filename))
		{
			if(new_dir.has_bsa(filename))
			{
				std::cout << "BSA file '" << filename << "' already registered! Skipping." << std::endl;
			}
			else
			{

				BSAFile mod_entry = BSAFile(filename);
				std::cout << "BSA file '" << mod_entry.name << "' registered." << std::endl;
				new_dir.bsas.push_back(mod_entry);
			}
		}
		else
		{
			std::cout << "File '" << filename << "' is not a mod or BSA! Skipping." << std::endl;
		}
	}

	return 0;
}

int m3_install::run(int argc, char** argv)
{

	int exitcode = 0;

	if(argc < 2)
	{
		this->usage(argv[0]);
		return 1;
	}

	M3ConfigFile config("m3_config.cfg");

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	int i;
	for(i=1; i<argc; i++)
	{
		exitcode = this->install(argv[i], config);
	}

	config.save("m3_config.cfg");

	return exitcode;

}

int m3_install::help(char* cmdname)
{
	return 0;
}
void m3_install::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << " <mod files and dirs...>" << std::endl;
}