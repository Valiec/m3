#ifndef CMD_CATCFG
#define CMD_CATCFG

#include "command.h"
#include "m3_config.h"

class m3_catcfg: public Command
{
public:
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif