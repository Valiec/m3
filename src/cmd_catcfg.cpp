#include <iostream>
#include <string>

#include "cmd_catcfg.h"

int m3_catcfg::run(int argc, char** argv)
{
	M3ConfigFile config("m3_config.cfg");

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	config.print();
	return 0;

}

int m3_catcfg::help(char* cmdname)
{
	return 0;
}
void m3_catcfg::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << std::endl;
}