#ifndef M3_INIT
#define M3_INIT

#include "command.h"
#include "omw_config.h"
#include "m3_config.h"

class m3_init: public Command
{
public:
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};

#endif