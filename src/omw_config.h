#ifndef OMW_CONFIG
#define OMW_CONFIG

#include <string>
#include <vector>
#include <unordered_map>
#include "config_utils.h"

class OMWConfigFile
{
public:
	std::string path;
	std::unordered_map<std::string, std::string> misc_settings;
	std::vector<std::string> misc_setting_keys;
	std::vector<std::string> fallback_archives;
	std::vector<std::string> fallbacks;
	std::vector<std::string> data_folder_names;
	std::vector<std::string> content_list;
	int status;

	OMWConfigFile(std::string filename);
	void print();
	int parse(std::string filename);
	int save(std::string filename);
private:
	void format(std::ostream &f);
};


class OMWConfigLine
{
public:
	std::string name;
	std::string value;

	OMWConfigLine(std::string name, std::string value);
	OMWConfigLine(std::string linebuf);
};

OMWConfigLine read_line(std::string linebuf);

void print_omw_config(OMWConfigFile &config_file);

int parse_omw_config(OMWConfigFile &config_file, std::string filename);

#endif