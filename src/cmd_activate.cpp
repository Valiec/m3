#include <iostream>
#include <string>

#include "cmd_activate.h"

int m3_activate::activate(char* mod, M3ConfigFile &config, OMWConfigFile &omw_config)
{
	bool isDir = true;
	bool isBSA = false;
	std::string name(mod);

	std::string dir_name = name;

	if(name.find_first_of("/\\") != std::string::npos)
	{
		isDir = false;
		dir_name = std::string(name, 0, name.find_first_of("/\\"));
		name = std::string(name, name.find_first_of("/\\")+1);
	}

	if(is_bsa(name))
	{
		isBSA = true;
	}

	int datadir_index = -1;
	int j;

	if(!isDir)
	{
		if(!isBSA)
		{
			std::vector<ContentFile> content = config.data_folders.at(dir_name).content;
			for(j=0; j<content.size(); j++)
			{
				if(content[j].name == name)
				{
					datadir_index = j;
				}
			}
		}
		else
		{
			std::vector<BSAFile> bsas = config.data_folders.at(dir_name).bsas;
			for(j=0; j<bsas.size(); j++)
			{
				if(bsas[j].name == name)
				{
					datadir_index = j;
				}
			}
		}
	}
	else
	{
		std::vector<std::string> folders = config.data_folder_names;
		for(j=0; j<folders.size(); j++)
		{
			if(folders[j] == name)
			{
				datadir_index = j;
			}
		}
	}

	if(datadir_index == -1)
	{
		std::cerr << "error: Cannot activate '" << mod << "' as no such " << (isDir ? "folder" : "file") << " exists." << std::endl;
		return 1;
	}

	int content_index = -1;


	if(!isDir)
	{
		if(!isBSA)
		{
			for(j=0; j<omw_config.content_list.size(); j++)
			{
				if(omw_config.content_list[j] == name)
				{
					content_index = j;
				}
			}
		}
		else
		{
			for(j=0; j<omw_config.fallback_archives.size(); j++)
			{
				if(omw_config.content_list[j] == name)
				{
					content_index = j;
				}
			}
		}
	}
	else
	{
		for(j=0; j<omw_config.data_folder_names.size(); j++)
		{
			if(omw_config.data_folder_names[j] == config.data_folders.at(dir_name).path)
			{
				content_index = j;
			}
		}	
	}


	if(content_index != -1)
	{
		std::cerr << "error: Cannot activate '" << name << "' as there is already a " << (isDir ? "folder" : "file") << " active with that name." << std::endl;
		return 1;
	}

	if(!isDir)
	{
		if(!isBSA)
		{
			config.data_folders.at(dir_name).content[datadir_index].activate();
		}
		else
		{
			config.data_folders.at(dir_name).bsas[datadir_index].activate();	
		}
	}
	else
	{
		config.data_folders.at(dir_name).activate();
		std::vector<ContentFile> content = config.data_folders.at(dir_name).content;
	}

	std::cout << "Activated data  " << (isDir ? "folder " : "file ") << name << std::endl;
	return 0;
}

int m3_activate::run(int argc, char** argv)
{

	int exitcode = 0;

	if(argc < 2)
	{
		this->usage(argv[0]);
		return 1;
	}

	M3ConfigFile config("m3_config.cfg");

	OMWConfigFile omw_config(config.omw_config_path);

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	int i;
	for(i=1; i<argc; i++)
	{
		exitcode = this->activate(argv[i], config, omw_config);
	}

	config.apply(omw_config);

	omw_config.save(config.omw_config_path);

	config.save("m3_config.cfg");

	return exitcode;

}

int m3_activate::help(char* cmdname)
{
	return 0;
}
void m3_activate::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << " <mod files and dirs...>" << std::endl;
}