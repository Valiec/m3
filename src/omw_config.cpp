#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include "omw_config.h"

OMWConfigLine::OMWConfigLine(std::string namefield, std::string valuefield)
{
	this->name = namefield;
	this->value = valuefield;
}

OMWConfigLine::OMWConfigLine(std::string linebuf)
{
	int eq_index = linebuf.find('=');
	this->name = linebuf.substr(0, eq_index);
	this->value = linebuf.substr(eq_index+1);
}

int OMWConfigFile::save(std::string filename)
{
	std::fstream f;
	f.open(filename, std::fstream::out);
	if(!f.is_open())
	{
		return 1; //file could not be opened error code
	}

	this->format(f);

	return 0;
}

void OMWConfigFile::print()
{
	this->format(std::cout);
}

void OMWConfigFile::format(std::ostream &f)
{
	int i;
	int encoding_index = -1;
	for(i=0; i<this->misc_setting_keys.size(); i++)
	{
		if(misc_setting_keys[i] != "encoding")
		{
			f << misc_setting_keys[i] << "=" << this->misc_settings.at(misc_setting_keys[i]) << std::endl;
		}
		else
		{
			encoding_index = i;
		}
	}
	for(i=0; i<this->fallback_archives.size(); i++)
	{
		f << "fallback-archive=" << this->fallback_archives[i] << std::endl;
	}
	for(i=0; i<this->fallbacks.size(); i++)
	{
		f << "fallback=" << this->fallbacks[i] << std::endl;
	}
	if(encoding_index>0)
	{
		f << misc_setting_keys[encoding_index] << "=" << this->misc_settings.at(misc_setting_keys[encoding_index]) << std::endl;
	}
	for(i=0; i<this->data_folder_names.size(); i++)
	{
		f << "data=\"" << this->data_folder_names[i] << "\"" << std::endl;
	}
	for(i=0; i<this->content_list.size(); i++)
	{
		f << "content=" << this->content_list[i] << std::endl;
	}
}

OMWConfigFile::OMWConfigFile(std::string filename)
{
	this->status = this->parse(filename);
}

int OMWConfigFile::parse(std::string filename)
{

	std::fstream f;
	f.open(filename, std::fstream::in);
	if(!f.is_open())
	{
		return 1; //file could not be opened error code
	}

    this->path = std::filesystem::canonical(filename);

	std::string linestr;

	while(getline(f, linestr))
	{
		if(linestr.find('=') == std::string::npos)
		{
			continue;
		}
		OMWConfigLine line(linestr);


		if(line.name == "fallback-archive")
		{
			this->fallback_archives.push_back(line.value);
		}
		else if(line.name == "fallback")
		{
			this->fallbacks.push_back(line.value);
		}
		else if(line.name == "data")
		{
			int line_len = line.value.size();
			if(line.value[0] == '"' && line.value[line_len-1] == '"') //if path is quoted, get rid of the quotes
			{
				line.value = line.value.substr(1, line.value.size()-2);
			}
			this->data_folder_names.push_back(line.value);
		}
		else if(line.name == "content")
		{
			this->content_list.push_back(line.value);
		}
		else
		{
			this->misc_settings.insert_or_assign(line.name, line.value);
			this->misc_setting_keys.push_back(line.name);
		}
	}
	f.close();
	return 0;
}