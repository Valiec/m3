#include <iostream>
#include <string>
#include <filesystem> 

#include "cmd_check.h"

int m3_check::run(int argc, char** argv)
{

	M3ConfigFile config("m3_config.cfg");

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	int fail = 0;

	if(!std::filesystem::exists(std::filesystem::path(config.omw_config_path)))
	{
		std::cerr << "fail: OMW config at " << config.omw_config_path << " not found!" << std::endl;
		fail++;
	}

	if(!std::filesystem::exists(std::filesystem::path(config.base_game_path)))
	{
		std::cerr << "fail: OMW main data directory at " << config.base_game_path << " not found!" << std::endl;
		fail++;
	}

	int i;
	for(i=0; i<config.data_folder_names.size(); i++)
	{
		DataFolder data_folder = config.data_folders.at(config.data_folder_names[i]);
		if(!std::filesystem::exists(std::filesystem::path(data_folder.path)))
		{
			std::cerr << "fail: data folder " << config.data_folder_names[i] << " not found at " << std::filesystem::path(data_folder.path) << "!" << std::endl;
			fail++;
		}
		else
		{
			int j;
			for(j=0; j<data_folder.content.size(); j++)
			{
				std::filesystem::path content_path = std::filesystem::path(data_folder.path) / data_folder.content[j].name;
				if(!std::filesystem::exists(std::filesystem::path(content_path)))
				{
					std::cerr << "fail: content file " << data_folder.content[j].name << " not found at " << content_path << "!" << std::endl;
					fail++;
				}	
			}

			for(j=0; j<data_folder.bsas.size(); j++)
			{
				std::filesystem::path bsa_path = std::filesystem::path(data_folder.path) / data_folder.bsas[j].name;
				if(!std::filesystem::exists(std::filesystem::path(bsa_path)))
				{
					std::cerr << "fail: BSA " << data_folder.bsas[j].name << " not found at " << bsa_path << "!" << std::endl;
					fail++;
				}
			}
		}
	}

	if(fail == 0)
	{
		std::cerr << "success: all checks passed" << std::endl;
	}

	return fail;

}

int m3_check::help(char* cmdname)
{
	return 0;
}
void m3_check::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << std::endl;
}