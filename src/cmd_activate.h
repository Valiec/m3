#ifndef CMD_ACTIVATE
#define CMD_ACTIVATE

#include "command.h"
#include "m3_config.h"

class m3_activate: public Command
{
public:
	int activate(char* mod, M3ConfigFile &config, OMWConfigFile &omw_config);
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif