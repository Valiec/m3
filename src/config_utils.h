#ifndef CONFIG_UTILS
#define CONFIG_UTILS

#include <string>
#include <vector>
#include <unordered_map>

class ContentFile
{
public:
	std::string name;
	std::string folder_name;
	std::string extension;
	bool active;
	bool isBaseGame;

	ContentFile(std::string name);
	//void setFolderName(std::string);
	void activate();
	void deactivate();
};

class BSAFile
{
public:
	std::string name;
	std::string folder_name;
	bool active;
	bool isBaseGame;

	BSAFile(std::string name);
	void activate();
	void deactivate();
};


class DataFolder
{
public:
	std::string name;
	std::string path;
	std::vector<ContentFile> content;
	std::vector<BSAFile> bsas;
	bool active;

	DataFolder(std::string name, std::string path);
	DataFolder(std::string name);
	bool has_mod(std::string name);
	bool has_bsa(std::string name);
	//std::vector<std::string> getContentNames();
	void activate();
	void deactivate();	
};


bool str_endswith(std::string str, std::string substr);

int match_mod_to_directory(ContentFile &mod, std::vector<std::string> &dir_names, std::unordered_map<std::string, DataFolder> &dirs);

bool is_base_morrowind(std::string name);

void lowercaseStr(std::string &str);

bool is_valid_mod_name(std::string str);

bool is_bsa(std::string str);

#endif