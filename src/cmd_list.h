#ifndef CMD_LIST
#define CMD_LIST

#include "command.h"
#include "m3_config.h"

class m3_list: public Command
{
public:
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif