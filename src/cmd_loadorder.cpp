#include <iostream>
#include <string>
#include <filesystem>
#include <cctype>
#include <tuple>
#include <vector>
#include <iterator>
#include <cmath>
#include <cstring>

#include "cmd_loadorder.h"

void print_load_order(M3ConfigFile &config, bool isBSA, std::vector<std::pair<std::string, int>>& load_order)
{
	int i, j, k;
	k = 1;
	int max_num_length = (int)std::floor(std::log10(load_order.size()))+1; //the number of digits in the largest number in the list
	for(i=0; i<load_order.size(); i++)
	{
		DataFolder data_folder = config.data_folders.at(load_order[i].first);
		std::filesystem::path content_path;
		if(!isBSA)
		{
			content_path = std::filesystem::path(data_folder.path) / data_folder.content[load_order[i].second].name;
		}
		else
		{
			content_path = std::filesystem::path(data_folder.path) / data_folder.bsas[load_order[i].second].name;
		}
		std::string activestr = "";
		std::string folder_activestr = "";
		if(!(data_folder.active))
		{
			folder_activestr = "[FOLDER INACTIVE] ";
		}
		if((!isBSA && !(data_folder.content[load_order[i].second].active)) && (isBSA && !(data_folder.bsas[load_order[i].second].active)))
		{
			activestr = "[INACTIVE] ";
		}
		std::string spacestr = "";
		int num_length = (int)std::floor(std::log10(k))+1; //the number of digits in the current number
		for(j=0; j<max_num_length-num_length; j++)
		{
			spacestr += ' ';
		}
		if(!isBSA)
		{
			std::cout << spacestr << k << ". " << activestr << folder_activestr << data_folder.content[load_order[i].second].name << " -- (" << data_folder.name << ") " << std::endl; 
		}
		else
		{
			std::cout << spacestr << k << ". " << activestr << folder_activestr << data_folder.bsas[load_order[i].second].name << " -- (" << data_folder.name << ") " << std::endl; 
		}
		k++;
	}
}

void apply_change(std::tuple<int, int, char> instruction, M3ConfigFile &config, std::vector<std::pair<std::string, int>>& load_order)
{
	int src = std::get<0>(instruction)-1;
	int dest = std::get<1>(instruction)-1;
	if(std::get<2>(instruction) == '+')
	{
		dest++;
	}
	load_order.insert(std::next(load_order.begin(), dest),load_order[src]);
	if(dest < src)
	{
		src++;	
	}
	load_order.erase(std::next(load_order.begin(),src));
}

bool is_numeric(std::string str)
{
	int i;
	for(i=0; i<str.length(); i++)
	{
		if(!std::isdigit((unsigned char)str[i]))
		{
			return false;
		}
	}
	return true;
}

int m3_loadorder::run(int argc, char** argv)
{
	M3ConfigFile config("m3_config.cfg");

	bool isBSA = false;

	if(argc > 1 && argv[1])
	{
		if(strcmp(argv[1], "--bsa") == 0 || strcmp(argv[1], "-b") == 0)
		{
			isBSA = true;
		}
	}

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	std::vector<std::pair<std::string, int>>& load_order = isBSA ? config.bsa_load_order : config.load_order;

	print_load_order(config, isBSA, load_order);
	
	std::string l;
	std::vector<std::tuple<int, int, char>> instructions;
	bool do_apply = false;
	while(!std::cin.eof())
	{
		std::cout << "> " << std::flush;
		getline(std::cin, l);
		int first_space = l.find(" ");
		std::string cmd = l.substr(0, first_space);
		std::string args = l.substr(first_space+1);
		std::string arg1, arg2;
		int src, dest;
		char direction = '+'; //'+' -> after, '-' -> before

		if(cmd == "m" || cmd == "mv" || cmd == "move")
		{
			if(first_space == std::string::npos)
			{
				std::cout << "error: missing argument" << std::endl;
				continue;
			}
			int arg_space = args.find(" ");
			if(arg_space == std::string::npos)
			{
				std::cout << "error: missing destination argument" << std::endl;
				continue;
			}
			arg1 = args.substr(0, arg_space);
			arg2 = args.substr(arg_space+1);

			int next_space = arg2.find(" ");
			if(next_space != std::string::npos)
			{
				std::string dir = arg2.substr(0, next_space);
				arg2 = arg2.substr(next_space+1);
				if(dir == "before" || dir == "-")
				{
					direction = '-';
				}
				else if(dir == "after" || dir == "+" || dir == "to")
				{
					direction = '+';
				}
				else
				{
					std::cout << "error: invalid direction " << dir << std::endl;
					continue;
				}
			}
			else if(arg2[0] == '-' || arg2[0] == '+')
			{
					if(arg2[0] == '-')
					{
						direction = '-';
						arg2 = arg2.substr(1);
					}
			}
			else
			{
				//nothing
			}
			if(!is_numeric(arg1))
			{
				std::cout << "error: non-numeric index " << arg1 << std::endl;
				continue;
			}
			if(!is_numeric(arg2))
			{
				std::cout << "error: non-numeric index " << arg2 << std::endl;
				continue;
			}
			src = std::stoi(arg1);
			dest = std::stoi(arg2);
			if(src == dest)
			{
				std::cout << "error: destination index equals source index" << std::endl;
				continue;
			}
			if(src < 1 || src > load_order.size())
			{
				std::cout << "error: source index out of range" << std::endl;
				continue;
			}
			if(dest < 1 || dest > load_order.size())
			{
				std::cout << "error: source index out of range" << std::endl;
				continue;
			}
			std::cout << "moving " << src << " to " << ((direction == '-' ) ? "before" : "after") << " " << dest << std::endl; 
			instructions.push_back(std::tuple<int, int, char>(src, dest, direction));
			apply_change(instructions.back(), config, load_order);
		}
		else if(cmd == "?" || cmd == "help")
		{
			if(first_space <= 0)
			{
				std::cout << "commands:" << std::endl;
				std::cout << "  m: move load order entry to a new position" << std::endl;
				std::cout << "  p: display load order" << std::endl;
				std::cout << "  u: undo last edit" << std::endl;
				std::cout << "  s: save and quit" << std::endl;
				std::cout << "  q: quit without saving" << std::endl;
				std::cout << "  ?: get help" << std::endl;
				std::cout << "Type ? <command> for further help on a command." << std::endl;

			}
			else
			{
				std::string helpcmd = args;
				if(helpcmd == "m" || helpcmd == "mv" || helpcmd == "move")
				{
					std::cout << "m: move load order entry to a new position" << std::endl;
					std::cout << "  aliases: mv, move" << std::endl;
					std::cout << "  usage: m <SRC> [+-]<DEST>" << std::endl;
					std::cout << "    Move line SRC to after line DEST" << std::endl;
					std::cout << "    (or before DEST if DEST is prefixed with '-')." << std::endl;
					std::cout << "    Prefixing DEST with '+' works the same as no prefix." << std::endl;
					std::cout << "  usage: m <SRC> [to|after|before] <DEST>" << std::endl;
					std::cout << "    Behaves identically to the above syntax," << std::endl;
					std::cout << "    with 'before' acting like '-'," << std::endl;
					std::cout << "    and 'after' and 'to' acting like '+' or no prefix." << std::endl;
				}
				else if(helpcmd == "p" || helpcmd == "print" || helpcmd == "v" || helpcmd == "view" || helpcmd == "d" || helpcmd == "display")
				{
					std::cout << "p: display load order" << std::endl;
					std::cout << "  aliases: v, d, print, view, display" << std::endl;
					std::cout << "  usage: v" << std::endl;
					std::cout << "    Outputs the entire load order with line numbers." << std::endl;
				}
				else if(helpcmd == "u" || helpcmd == "undo")
				{
					std::cout << "p: undo last edit" << std::endl;
					std::cout << "  aliases: undo" << std::endl;
					std::cout << "  usage: u" << std::endl;
					std::cout << "    Undoes the last edit performed." << std::endl;
				}
				else if(helpcmd == "s" || helpcmd == "save")
				{
					std::cout << "s: save and quit" << std::endl;
					std::cout << "  aliases: save" << std::endl;
					std::cout << "  usage: s" << std::endl;
					std::cout << "    Saves changes and exits." << std::endl;
				}
				else if(helpcmd == "q" || helpcmd == "quit")
				{
					std::cout << "q: quit without saving" << std::endl;
					std::cout << "  aliases: quit" << std::endl;
					std::cout << "  usage: q" << std::endl;
					std::cout << "    Quits without saving" << std::endl;
					std::cout << "  The EOF character acts identically to the q command." << std::endl;
				}
				else if(helpcmd == "?" || helpcmd == "help")
				{
					std::cout << "?: get help" << std::endl;
					std::cout << "  aliases: help" << std::endl;
					std::cout << "  usage: ?" << std::endl;
					std::cout << "    Prints a list of editor commands." << std::endl;
					std::cout << "  usage: ? <command>" << std::endl;
					std::cout << "    Prints detailed help on the specified command." << std::endl;
				}
				else
				{
					std::cout << "unknown command '" << helpcmd << "', type ? to list all commands" << std::endl;
				}
			}

			

		}
		else if(cmd == "")
		{
			//do nothing
		}
		else if(cmd == "p" || cmd == "print" || cmd == "v" || cmd == "view" || cmd == "d" || cmd == "display")
		{
			print_load_order(config, isBSA, load_order);
		}
		else if(cmd == "u" || cmd == "undo")
		{
			if(instructions.size() == 0)
			{
				std::cout << "error: nothing to undo" << std::endl;	
			}
			else
			{
				std::tuple<int, int, char> last = instructions.back();
				int new_src = std::get<1>(last);
				int src = std::get<0>(last);
				if(new_src > src)
				{
					new_src--;
				}
				if(std::get<2>(last) == '+')
				{
					new_src++;
				}
				std::tuple<int, int, char> reverse(new_src, src, '-');
				apply_change(reverse, config, load_order);
				std::cout << "change undone" << std::endl;
				instructions.pop_back();
			}
		}
		else if(cmd == "s" || cmd == "save")
		{
			do_apply = true;
			break;
		}
		else if(cmd == "q" || cmd == "quit")
		{
			break;
		}
		else
		{	
			std::cout << "unknown command '" << cmd << "', type ? for help" << std::endl;
		}

	}

	if(instructions.size() > 0 && do_apply)
	{

		OMWConfigFile omw_config(config.omw_config_path);

		config.apply(omw_config);

		omw_config.save(config.omw_config_path);

		config.save("m3_config.cfg");

		std::cout << "changes saved" << std::endl;
	}
	else if(instructions.size() > 0 && !do_apply)
	{
		std::cout << "canceling changes" << std::endl;
	}
	else
	{
		std::cout << "no changes made" << std::endl;
	}

	return 0;

}

int m3_loadorder::help(char* cmdname)
{
	return 0;
}
void m3_loadorder::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << " [-b]" << std::endl;
}