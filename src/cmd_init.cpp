#include <iostream>
#include <string>

#include "cmd_init.h"

int m3_init::run(int argc, char** argv)
{
	if(argc < 2)
	{
		this->usage(argv[0]);
		return 1;
	}

	OMWConfigFile omw_config_file(argv[1]);

	if(omw_config_file.status == 1)
	{
		std::cerr << "error: could not open OpenMW config file '" << argv[1] << "'" << std::endl;
		return 1;
	}

	M3ConfigFile config(omw_config_file);

	config.save("m3_config.cfg");

	return 0;

}

int m3_init::help(char* cmdname)
{
	return 0;
}
void m3_init::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << " <openmw.cfg>" << std::endl;
}