#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include <set>
#include "m3_config.h"

ConfigLine::ConfigLine(std::string linebuf)
{
	int eq_index = linebuf.find('=');
	this->value = linebuf.substr(eq_index+1);
	this->name = linebuf.substr(0, eq_index);
	int last_colon_index = this->name.rfind(':');
	while(last_colon_index != std::string::npos)
	{
		this->tags.insert_or_assign(this->name.substr(last_colon_index+1), true);
		this->name = this->name.substr(0, last_colon_index);
		last_colon_index = this->name.rfind(':');
	}
}

M3ConfigFile::M3ConfigFile(OMWConfigFile cfg)
{
	std::vector<std::pair<std::string, int>> inactive_load_order;
	std::vector<std::pair<std::string, int>> inactive_bsa_load_order;

	std::set<std::pair<std::string, int>> seen_load_order;
	std::set<std::pair<std::string, int>> seen_bsa_load_order;

	this->omw_config_path = cfg.path;
	bool basePathSet = false;
	int i, j;
	std::unordered_map<std::string, std::pair<std::string, int>> bsa_map;
	for(i=0; i<cfg.data_folder_names.size(); i++)
	{
		DataFolder folder(std::filesystem::path(cfg.data_folder_names[i]).filename().string(), cfg.data_folder_names[i]);
		folder.activate();
		this->add_data_folder(folder);
		std::filesystem::directory_iterator dir_iter(std::filesystem::path(cfg.data_folder_names[i]));
		for(std::filesystem::directory_entry dir_entry : dir_iter)
		{
			if(dir_entry.is_regular_file())
			{
				if(is_valid_mod_name(dir_entry.path().filename()))
				{
					ContentFile contentF(dir_entry.path().filename());
					contentF.deactivate();
					contentF.isBaseGame = is_base_morrowind(dir_entry.path().filename());
					contentF.folder_name = folder.name;
					this->data_folders.at(folder.name).content.push_back(contentF);
					std::pair<std::string, int> load_pair(folder.name, this->data_folders.at(folder.name).content.size()-1);
					inactive_load_order.push_back(load_pair);
				}
				if(is_bsa(dir_entry.path().filename()))
				{
					BSAFile bsaF(dir_entry.path().filename());
					bsaF.deactivate();
					bsaF.isBaseGame = is_base_morrowind(dir_entry.path().filename());
					bsaF.folder_name = folder.name;
					this->data_folders.at(folder.name).bsas.push_back(bsaF);
					std::pair<std::string, int> load_pair(folder.name, this->data_folders.at(folder.name).bsas.size()-1);
					bsa_map.insert_or_assign(dir_entry.path().filename(), load_pair);
					inactive_bsa_load_order.push_back(load_pair);
				}
			}
		}
	}
	for(i=0; i<cfg.fallback_archives.size(); i++)
	{
		std::string bsaName = cfg.fallback_archives[i];
		std::pair<std::string, int> load_pair = bsa_map.at(bsaName);
		if(this->data_folders.count(load_pair.first) > 0)
		{
			BSAFile bsa = this->data_folders.at(load_pair.first).bsas[load_pair.second];
			bsa.activate();
			this->bsa_load_order.push_back(load_pair);
			seen_bsa_load_order.insert(load_pair);
		}
		else
		{
			std::cerr << "warn: could not find BSA '" << bsaName << "', skipping" << std::endl;
		}
	}
	for(i=0; i<cfg.content_list.size(); i++)
	{
		ContentFile contentF(cfg.content_list[i]);
		contentF.activate();
		contentF.isBaseGame = is_base_morrowind(cfg.content_list[i]);
		int folder_index = match_mod_to_directory(contentF, this->data_folder_names, this->data_folders);
		if(contentF.isBaseGame && !basePathSet)
		{
			this->base_game_path = this->data_folders.at(this->data_folder_names[folder_index]).path;
		}
		if(folder_index >= 0)
		{
			for(j=0; j < this->data_folders.at(this->data_folder_names[folder_index]).content.size(); j++)
			{
				ContentFile &file = this->data_folders.at(this->data_folder_names[folder_index]).content[j];
				if(file.name == cfg.content_list[i])
				{
					file.activate();
					std::pair<std::string, int> load_pair(this->data_folder_names[folder_index], j);
					this->load_order.push_back(load_pair);
					seen_load_order.insert(load_pair);
				}
			}
		}
		else
		{
			std::cerr << "warn: could not find mod '" << contentF.name << "', skipping" << std::endl;
		}
	}
	for(std::pair<std::string, int> load_pair : inactive_load_order)
	{
		if(seen_load_order.count(load_pair) == 0)
		{
			this->load_order.push_back(load_pair);
		}
	}
	for(std::pair<std::string, int> load_pair : bsa_load_order)
	{
		if(seen_bsa_load_order.count(load_pair) == 0)
		{
			this->bsa_load_order.push_back(load_pair);
		}
	}
}

int M3ConfigFile::save(std::string filename)
{
	std::fstream f;
	f.open(filename, std::fstream::out);
	if(!f.is_open())
	{
		return 1; //file could not be opened error code
	}

	this->format(f);

	return 0;
}


void M3ConfigFile::print()
{
	this->format(std::cout);
}


void M3ConfigFile::format(std::ostream &f)
{

	f << "omw-config-path=" << this->omw_config_path << std::endl;
	f << "base-data-path=" << this->base_game_path << std::endl;
	
	for(int i=0; i<this->data_folder_names.size(); i++)
	{
		DataFolder folder = this->data_folders.at(this->data_folder_names[i]);
		f << "data-folder";
		if(folder.active)
		{
			f << ":active";
		}
		f << "=" << folder.name << std::endl;
		f << "path=" << folder.path << std::endl;

		for(int j=0; j<folder.content.size(); j++)
		{
			f << "content";
			if(folder.content[j].isBaseGame)
			{
				f << ":base";
			}
			if(folder.content[j].active)
			{
				f << ":active";
			}
			f << "=" << folder.content[j].name << std::endl;
		}
		for(int i=0; i<folder.bsas.size(); i++)
		{
			f << "bsa";
			if(folder.bsas[i].isBaseGame)
			{
				f << ":base";
			}
			if(folder.bsas[i].active)
			{
				f << ":active";
			}
			f << "=" << folder.bsas[i].name << std::endl;
		}

	}

	for(int i=0; i<this->load_order.size(); i++)	
	{
		f << "load-order" << "=" << this->load_order[i].first << "/" << this->data_folders.at(this->load_order[i].first).content[this->load_order[i].second].name << std::endl;
	}
	for(int i=0; i<this->bsa_load_order.size(); i++)	
	{
		f << "load-order:bsa" << "=" << this->bsa_load_order[i].first << "/" << this->data_folders.at(this->bsa_load_order[i].first).bsas[this->bsa_load_order[i].second].name << std::endl;
	}

}

M3ConfigFile::M3ConfigFile(std::string filename)
{
	this->status = this->parse(filename);
}

int M3ConfigFile::parse(std::string filename)
{

	std::fstream f;
	f.open(filename, std::fstream::in);
	if(!f.is_open())
	{
		return 1; //file could not be opened error code
	}

	std::string linestr;

	while(getline(f, linestr))
	{
		ConfigLine line(linestr);

		if(line.name == "omw-config-path")
		{
			this->omw_config_path = line.value;
		}
		else if(line.name == "base-data-path")
		{
			this->base_game_path = line.value;
		}
		else if(line.name == "data-folder")
		{
			this->data_folder_names.push_back(line.value);
			this->data_folders.insert_or_assign(line.value, DataFolder(line.value));
			if(line.tags.count("active") > 0)
			{
				this->data_folders.at(this->data_folder_names.back()).activate();
			}
		}
		else if(line.name == "path")
		{
			this->data_folders.at(this->data_folder_names.back()).path = line.value;
		}
		else if(line.name == "content")
		{
			ContentFile content(line.value);
			content.isBaseGame = false;
			if(line.tags.count("active") > 0)
			{
				content.activate();
			}
			if(line.tags.count("base") > 0)
			{
				content.isBaseGame = true;
			}
			this->data_folders.at(this->data_folder_names.back()).content.push_back(content);
		}
		else if(line.name == "bsa")
		{
			BSAFile bsa(line.value);
			bsa.isBaseGame = false;
			if(line.tags.count("active") > 0)
			{
				bsa.activate();
			}
			if(line.tags.count("base") > 0)
			{
				bsa.isBaseGame = true;
			}
			this->data_folders.at(this->data_folder_names.back()).bsas.push_back(bsa);
		}
		else if(line.name == "load-order")
		{
			bool isBSA = false;
			if(line.tags.count("bsa") > 0)
			{
				isBSA = true;
			}
			int slash_index = line.value.find('/');
			std::string folder = line.value.substr(0, slash_index);
			std::string file = line.value.substr(slash_index+1);

			int file_index = -1;

			int i;
			if(!isBSA)
			{
				for(i=0; i<this->data_folders.at(folder).content.size(); i++)
				{
					if(this->data_folders.at(folder).content[i].name == file)
					{
						std::pair<std::string, int> entry(folder, i);
						this->load_order.push_back(entry);
					}
				}
			}
			else
			{
				for(i=0; i<this->data_folders.at(folder).bsas.size(); i++)
				{
					if(this->data_folders.at(folder).bsas[i].name == file)
					{
						std::pair<std::string, int> entry(folder, i);
						this->bsa_load_order.push_back(entry);
					}
				}
			}
		}
		else
		{
			std::cerr << "warn: unknown config entry '" << line.name << "'" << std::endl << "line is: " << linestr << std::endl;
		}
	}
	f.close();
	return 0;
}

bool M3ConfigFile::has_data_folder(std::string dir_path)
{
	return this->get_data_folder(dir_path) != "";
}

std::string M3ConfigFile::get_data_folder(std::string dir_path)
{
	for(auto pair : this->data_folders)
	{
		if(pair.second.path == dir_path)
		{
			return pair.first;
		}
	}
	return "";
}

std::string M3ConfigFile::add_data_folder(DataFolder& folder)
{
	if(this->data_folders.count(folder.name) == 1)
	{
		std::filesystem::path folderPath(folder.path);
		std::string name(folder.name);
		while(this->data_folders.count(name) == 1)
		{
			folderPath = folderPath.parent_path();
			name = (folderPath.filename().string())+"_"+name;
		}
		folder.name = name;
	}
	this->data_folder_names.push_back(folder.name);
	this->data_folders.insert_or_assign(folder.name, folder);
	return folder.name;
}

void M3ConfigFile::remove_data_folder(DataFolder& folder)
{
	if(this->data_folders.count(folder.name) == 1)
	{
		for(int i=0; i<this->data_folder_names.size(); i++)
		{
			if(folder.name == this->data_folder_names[i])
			{
				this->data_folder_names.erase(this->data_folder_names.begin()+i);
			}
		}
		this->data_folders.erase(folder.name);
	}
}


void M3ConfigFile::apply(OMWConfigFile &omw_config)
{
	int i;
	omw_config.fallback_archives.clear();

	omw_config.data_folder_names.clear();
	omw_config.content_list.clear();

	for(i=0; i<this->data_folder_names.size(); i++)
	{
		std::string data_folder_name = this->data_folder_names[i];
		if(this->data_folders.at(data_folder_name).active)
		{
			omw_config.data_folder_names.push_back(this->data_folders.at(data_folder_name).path);
		}
	}
	for(i=0; i<this->load_order.size(); i++)
	{
		if(this->data_folders.at(this->load_order[i].first).content[this->load_order[i].second].active && this->data_folders.at(this->load_order[i].first).active)
		{
			omw_config.content_list.push_back(this->data_folders.at(this->load_order[i].first).content[this->load_order[i].second].name);
		}
	}
	for(i=0; i<this->bsa_load_order.size(); i++)
	{
		if(this->data_folders.at(this->bsa_load_order[i].first).content[this->bsa_load_order[i].second].active && this->data_folders.at(this->bsa_load_order[i].first).active)
		{
			omw_config.fallback_archives.push_back(this->data_folders.at(this->bsa_load_order[i].first).content[this->bsa_load_order[i].second].name);
		}
	}
}