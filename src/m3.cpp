#include <iostream>
#include <cstring>
#include "cmd_init.h"
#include "cmd_catcfg.h"
#include "cmd_check.h"
#include "cmd_list.h"
#include "cmd_activate.h"
#include "cmd_deactivate.h"
#include "cmd_loadorder.h"
#include "cmd_install.h"

int main(int argc, char** argv)
{

	m3_init init;
	m3_catcfg catcfg;
	m3_check check;
	m3_list list;
	m3_activate activate;
	m3_deactivate deactivate;
	m3_loadorder loadorder;
	m3_install install;
	std::unordered_map<std::string, Command*> commands = {{"init", &init}, 
														  {"catcfg", &catcfg}, 
														  {"check", &check}, 
														  {"list", &list}, 
														  {"activate", &activate}, 
														  {"enable", &activate}, 
														  {"deactivate", &deactivate}, 
														  {"disable", &deactivate}, 
														  {"loadorder", &loadorder}, 
														  {"install", &install},
														};

	if(argc < 2)
	{
		std::cerr << "usage: " << TOOL_NAME << " <command> [arguments]" << std::endl;
		return 1;
	}

	//removes the 1st element of argv
	char* cmd = argv[1];
	argv++;

	if(commands.count(cmd) > 0)
	{
		commands.at(cmd)->run(argc-1, argv);
	}		
	else
	{
		std::cerr << TOOL_NAME << ": unknown command: " << cmd << std::endl;
		return 1;
	}

}