#include <iostream>
#include <string>
#include <filesystem>

#include "cmd_remove.h"

int m3_remove::remove(char* mod, M3ConfigFile &config, OMWConfigFile &omw_config)
{


	std::string mod_path_str(mod);

	if(mod_path_str.back() == '/' || mod_path_str.back() == '\\')
	{
		mod_path_str.pop_back(); //get rid of a trailing \ or /
	}	

	std::filesystem::path mod_path(mod_path_str);

	if(!std::filesystem::exists(mod_path))
	{
		std::cerr << "error: path '" << mod_path_str << "' does not exist" << std::endl;
		return 1;
	}

	bool is_dir = std::filesystem::is_directory(mod_path);

	std::string filename;
	std::string dirname;
	std::string dirpath;	

	if(!is_dir)
	{

		filename = mod_path.filename().string();
		dirname = mod_path.parent_path().filename().string();
		dirpath = mod_path.parent_path().string();
	}

	else
	{
		filename = "";
		dirname = mod_path.filename().string();
		dirpath = mod_path.string();
	}

	if(is_dir)
	{
		if(config.has_data_folder(std::filesystem::canonical(dirpath)))
		{
			std::string data_folder_name = config.get_data_folder(std::filesystem::canonical(dirpath));
			DataFolder new_dir = config.data_folders.at(data_folder_name);
			config.remove_data_folder(new_dir);
			std::cout << "Folder '" << dirpath << "' removed." << std::endl;
		}
		else
		{
			std::cerr << "Folder '" << dirpath << "' not found! Skipping." << std::endl;
		}
	}
	else
	{
		if(!config.has_data_folder(std::filesystem::canonical(dirpath)))
		{
			std::cerr << "Containing folder '" << dirpath << "' for mod '" << filename << "' not found! Skipping." << std::endl;
		}
		std::string data_folder_name = config.get_data_folder(std::filesystem::canonical(dirpath));
		DataFolder new_dir = config.data_folders.at(data_folder_name);


		if(!new_dir.has_mod(filename))
		{
			std::cout << "Content file '" << filename << "' not found! Skipping." << std::endl;
		}
		else
		{
			if(is_valid_mod_name(filename))
			{
				ContentFile mod_entry = ContentFile(filename);
				for(int i=0; i<new_dir.content.size(); i++)
				{
					if(new_dir.content[i].name == mod_entry.name)
					{
						new_dir.content.erase(new_dir.content.begin()+i);
						break;
					}
				}
				std::cout << "Content file '" << mod_entry.name << "' removed." << std::endl;
			}
			else if(is_bsa(filename))
			{
				BSAFile mod_entry = BSAFile(filename);
				for(int i=0; i<new_dir.bsas.size(); i++)
				{
					if(new_dir.bsas[i].name == mod_entry.name)
					{
						new_dir.bsas.erase(new_dir.bsas.begin()+i);
						break;
					}
				}
				std::cout << "BSA file '" << mod_entry.name << "' removed." << std::endl;
			}
			else
			{
				std::cout << "File '" << filename << "' is not a mod or BSA! Skipping." << std::endl;
			}
		}
	}
	
	return 0;
}

int m3_remove::run(int argc, char** argv)
{

	int exitcode = 0;

	if(argc < 2)
	{
		this->usage(argv[0]);
		return 1;
	}

	M3ConfigFile config("m3_config.cfg");

	OMWConfigFile omw_config(config.omw_config_path);

	if(config.status == 1)
	{
		std::cerr << "error: could not open M3 config file" << std::endl;
		return 1;
	}

	int i;
	for(i=1; i<argc; i++)
	{
		exitcode = this->remove(argv[i], config, omw_config);
	}

	config.save("m3_config.cfg");

	return exitcode;

}

int m3_remove::help(char* cmdname)
{
	return 0;
}
void m3_remove::usage(char* cmdname)
{
	std::cerr << "usage: " << TOOL_NAME << " " << cmdname << " <mod files and dirs...>" << std::endl;
}