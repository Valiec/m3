#ifndef CMD_REMOVE
#define CMD_REMOVE

#include "command.h"
#include "m3_config.h"

class m3_remove: public Command
{
public:
	int remove(char* mod, M3ConfigFile &config, OMWConfigFile &omw_config);
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif