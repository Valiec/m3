#ifndef CMD_INSTALL
#define CMD_INSTALL

#include "command.h"
#include "m3_config.h"

class m3_install: public Command
{
public:
	int install(char* mod, M3ConfigFile &config);
	int run(int argc, char** argv);
	int help(char* cmdname);
	void usage(char* cmdname);
};


#endif