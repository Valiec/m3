# M3 OpenMW Mod Manager

M3 is a command-line mod manager tool for OpenMW Morrowind installations. It allows you to activate and deactivate mods (both `.esp`/`.esm` and `.omwaddon` formats), as well as `.bsa` archives in an installation, as well as configure the load order for both mods and BSAs. It fully supports multiple data folders for both mods and BSAs, and does not move any mods/BSAs around on the file system when installing/activating, but merely registers or unregisters data folders and content files.

## How to use

Documentation is WIP, to be added here. Also use `m3 help` to see the built-in help.

## Compatibility

This tool should work with any reasonable OpenMW Morrowind install. This likely includes TES3MP clients, as they are an OpenMW fork, but not the TES3MP server software.

## Limitations

M3 is a command-line application and does not have a GUI.

Additionally, M3 assumes you are running Morrowind in OpenMW. For instance, it assumes that the main/base data folder contains Morrowind.esm, Tribunal.esm, and Bloodmoon.esm, and considers any files by those names as "base game" files. If you're running something other than Morrowind, this is obviously not going to be correct.

## Trivia

### Name

The name M3 derived from a historical name for this tool, "MMM", short for "Morrowind Mod Manager". This name was obviously too generic (it doesn't even mention OpenMW, and this tool does *not* work with the Morrowind.exe engine so that's important to specify), so it was changed to M3 before release.

### History

This tool started as a Python script back in late 2021, which I abandoned and then restarted in C, then moved to C++ when I realized how much of the standard library I was recoding in the process.